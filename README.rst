===============
eCall interface
===============


.. image:: https://img.shields.io/pypi/v/ecall.svg
        :target: https://pypi.python.org/pypi/ecall

.. image:: https://img.shields.io/travis/audreyr/ecall.svg
        :target: https://travis-ci.org/audreyr/ecall

.. image:: https://readthedocs.org/projects/ecall/badge/?version=latest
        :target: https://ecall.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Webservice access for Dolphin's eCall SMS sending solution


* Free software: GNU General Public License v3
* Documentation: https://ecall.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
