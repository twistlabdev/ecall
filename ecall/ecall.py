# -*- coding: utf-8 -*-
"""eCall general config"""
import logging

logger = logging.getLogger(__name__)

WSDL = 'https://soap.ecall.ch/eCall.asmx?WSDL'
ECALL_MESSAGES = {
    0: "The message was sent",
    11000: "Syntax error",
    11001: "Access denied",
    11100: "Invalid or incorrect recipient address",
    11101: "Invalid or incorrect confirmation address",
    11102: "Missing recipient address",
    11103: "Missing notification address",
    11104: "Too many recipient addresses transmitted",
    11105: "Recipient address not within defined area",
    11200: "No message existent",
    11201: "Invalid characters in the message",
    11202: "Invalid sending time given",
    11203: "Sending time is not within time window",
    11204: "SMS/Pager only: Message is too long. In this case you have to check the setting \"maximum number of pages for long messages\" in eCall.",
    11300: "Unknown User",
    11301: "Not enough credits in account",
    11303: "Not enough free credits in account",
    11400: "Invalid call system",
    11401: "Invalid call system for this user",
    11402: "Call system is not supported",
    11403: "Call system is temporarily out of order",
    11500: "ID is missing",
    11501: "ID could not be found",
    11502: "ID does not exist in the system",
    11600: "Message was already transmitted",
    11700: "Data contents could not be read",
    11800: "Callback contains a forbidden number or text",
    11904: "Too many attachments uploaded (max. 10 allowed)",
    11905: "At least 1 attachment is too large. Max. size for attachments is 5 MB",
    11906: "File type is not supported as attachment. The following types are supported by eCall: bmp, pdf, doc, rtf, ppt, dok, snp, gif, tif, tiff, html, txt, jpg, wir, jpeg, xls, zip",
    11907: "There was an unexpected internal error while processing the attachments",
    11908: "There was an unexpected internal error while saving the attachments.",
    11910: "Job could not be found in the log",
    11911: "In a job status request you have to submit a JobID",
    11912: "Job is not executed yet but scheduled",
    11913: "Job is currently in progress",
    11914: "The name of the attachment is too long",
    11999: "Unknown error",
}
SUCCESS_CODES = [0]
ACCESS_DENIED_CODES = [11001, 11300]
INVALID_DATA_CODES = [11100, 11101, 11102, 11103, 11104, 11105, 11200, 11201, 11202, 11203, 11204,
                      11500, 11501, 11502, ]
NOT_ENOUGH_CREDIT_CODES = [11301, 11303]
SCHEDULED_CODES = [11912, 11913]


class ECallException(BaseException):
    """There was an ambiguous exception that occurred while handling your request."""
    def __init__(self, *args, **kwargs):
        response = kwargs.pop('response', None)
        self.response = response


class ECallUnReachableException(ECallException):
    """NetworkError"""


class AccessDeniedException(ECallException):
    """Credentials are invalid"""


class InvalidDataException(ECallException):
    """Data is incorrect"""


class NotEnoughCreditException(ECallException):
    """Not enough credit in account"""
