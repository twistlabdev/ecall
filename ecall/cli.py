# -*- coding: utf-8 -*-
"""Console script for ecall."""
import os
import sys
import click

from .ecall import ECALL_MESSAGES, ECallException
from .sms import ECallSMSService


@click.command()
@click.option('-a', '--account', required=True, prompt=True, default=lambda: os.environ.get('ECALL_ACCOUNT'))
@click.option('-p', '--password', required=True, prompt=True, default=lambda: os.environ.get('ECALL_PASSWORD'))
@click.option('-n', '--number', required=True, prompt=True)
@click.option('-m', '--message', required=True, prompt=True)
@click.option('-c', '--callback', required=True, prompt=True)
def main(account, password, number, message, callback):
    """Console script for ecall."""
    e_service = ECallSMSService(account, password)
    try:
        e_service.send_sms(phone_number=number, message=message, callback=callback)
        click.echo("Message was sent to {}".format(number))
    except ECallException as e:
        click.echo("Message could not be sent: {}".format(ECALL_MESSAGES.get(e.response.ResponseCode)))
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
