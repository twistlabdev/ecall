# -*- coding: utf-8 -*-

"""Main module."""
import logging
import uuid

import requests
import zeep


from .ecall import (
    ECALL_MESSAGES, WSDL,
    SUCCESS_CODES, ACCESS_DENIED_CODES, INVALID_DATA_CODES, NOT_ENOUGH_CREDIT_CODES, SCHEDULED_CODES,
    ECallException, ECallUnReachableException, AccessDeniedException, InvalidDataException, NotEnoughCreditException
)

logger = logging.getLogger(__name__)


class ECallSMSService:
    """
    SMS sending service for eCall
    """
    def __init__(self, account_name, account_password, wsdl_location=WSDL):
        self.account_name = account_name
        self.account_password = account_password
        try:
            self.client = zeep.Client(wsdl=wsdl_location)
        except requests.exceptions.RequestException as requests_exception:
            raise ECallUnReachableException from requests_exception

    def send_sms(self, phone_number, message, job_id=None, send_date=None, callback=None):
        if not job_id:
            job_id = str(uuid.uuid4())
        try:
            response = self.client.service.SendSMSBasic(
                AccountName=self.account_name, AccountPassword=self.account_password,
                Address=phone_number, Message=message,
                JobID=job_id, SendDate=send_date and send_date.isoformat() or None,
                SMSCallback=callback
            )
        except requests.exceptions.RequestException as requests_exception:
            raise ECallUnReachableException from requests_exception
        except zeep.exceptions.Error as e:
            raise ECallException(response) from e
        if response.ResponseCode in SUCCESS_CODES:
            logger.debug('SMS sent to:'.format(phone_number))
            return job_id
        elif response.ResponseCode in ACCESS_DENIED_CODES:
            exception = AccessDeniedException
        elif response.ResponseCode in INVALID_DATA_CODES:
            exception = InvalidDataException
        elif response.ResponseCode in NOT_ENOUGH_CREDIT_CODES:
            exception = NotEnoughCreditException
        else:
            exception = ECallException
        logger.error('Could not send SMS: {}'.format(response.ResponseText))
        raise exception(response=response, message=ECALL_MESSAGES.get(response.ResponseCode))

    def get_status(self, job_id):
        """
        Get eCall status for job job_id
        :param job_id:
        :return: a JobResponseDetail, containing:
            Address (phone number),
            JobType (1 for SMS),
            SendState (8 for reception confirmed, 3 for scheduled, 1 for sent, 0 for waiting, see eCall doc)
            ErrorState (see eCall doc)
            PointsUsed
            FinishDate (sent datetime, not read)
        """
        try:
            response = self.client.service.GetStateBasic(
                AccountName=self.account_name, AccountPassword=self.account_password,
                JobID=job_id)
        except zeep.exceptions.Error() as exception:
            raise ECallException(response) from exception
        if response.ServiceResponse.ResponseCode in SUCCESS_CODES + SCHEDULED_CODES:
            return response.JobResponse
        else:
            logger.warning('Could not check status: {}'.format(response.ResponseText))
            return None
